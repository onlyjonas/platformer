﻿#pragma strict

/* SENSOR SCRIPT * * * * * * * * * * * * * * * * * * * *
On event calls an event if timer reaches timeTillTrigger 


* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventRecieveMessageTag : String;
public var startTimer : boolean;
public var randomTimeMin : float;
public var randomTimeMax : float;
public var loopTimer : boolean;

public var eventSendMessageTag : String;
public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var once : boolean;


function Update () {

	if(startTimer) {
		startTimer = false;
		myTimer();	
	}
	
}

function myTimer() {

	yield WaitForSeconds(Random.Range(randomTimeMin, randomTimeMax));
	
	SendMyEvent();
	
	if(loopTimer) startTimer = true;
}


function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventRecieveMessageTag) {
		
		startTimer = true;
	
	}
}
	
	
function SendMyEvent() {

		if(sendEventToMe) gameObject.SendMessage("MyEvent", eventSendMessageTag);

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent", eventSendMessageTag);
		}

}
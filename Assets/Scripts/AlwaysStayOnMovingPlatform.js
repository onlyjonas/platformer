﻿#pragma strict

/* ALWAYS SCRIPT * * * * * * * * * * * * * * * * * * * *
Moving platform script makes the player the child object
of the platform while the player is in the trigger area
of the RuntimePlatform.

NOTE: An additional Collider 2D (as trigger) is needed 
* * * * * * * * * * * * * * * * * * * * * * * * * * * */

function OnTriggerStay2D(other : Collider2D) {

	//if(other.gameObject.tag == "Player"){
		other.transform.parent = transform;
	//}

}
function OnTriggerExit2D(other : Collider2D){
	//if(other.gameObject.tag == "Player"){
		other.transform.parent = null;
	//}
} 
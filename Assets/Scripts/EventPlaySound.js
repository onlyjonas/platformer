﻿#pragma strict
@script RequireComponent(AudioSource)

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *

eventMessageTag : set event message tag, to only 
trigger event when eventMessageTag is send

sound : the sound to play

* * * * * * * * * * * * * * * * * * * * * * * * * * * */


public var eventMessageTag : String;
var sound : AudioClip;

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {
	
	
		AudioSource.PlayClipAtPoint (sound, transform.position);

	
	}
}




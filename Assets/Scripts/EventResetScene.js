﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event reset current scene

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		Application.LoadLevel (Application.loadedLevel);

	}
}
﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event swap object with target object

eventMessageTag : set event message tag, to only 
trigger event when specific tag is send

changeToPrefab : set a target prefab

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;
public var changeToPrefab : Transform;

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {
		var obj = Instantiate (changeToPrefab, transform.position, Quaternion.identity);
		obj.parent = transform.parent;
		Destroy (gameObject);
	}

}
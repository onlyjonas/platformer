﻿#pragma strict

public var eventMessageTag : String;
public var animationParameterID : int;

private var animator : Animator;

function Start () {
	
	animator = GetComponent(Animator);
	
}


function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {
		
		animator.SetInteger("animationID", animationParameterID);
	
	}
}
﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event move object a relative distance 

eventMessageTag : set event message tag, to only 
trigger event when specific tag is send

toggleMe : if true, object will move between start and target position 

speed : speed of the movement 

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;
public var moveRelativeDistance : Vector2;
public var toggleMe : boolean;
public var speed : float = 1.0; 

private var defaultPos : Vector2;
private var targetPos : Vector2;
private var toggle : boolean; 
private var startMoving : boolean = false;

function Start () {
	
	defaultPos = transform.position;
	

}

function Update () {

	if (startMoving) {

		var step = speed * Time.deltaTime;
	
		transform.position = Vector2.MoveTowards(Vector2(transform.position.x,transform.position.y), targetPos, step);

		if(Vector2.Distance(Vector2(transform.position.x,transform.position.y), targetPos) == 0) startMoving = false;
	
	}

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {
		if(!toggle) {
			targetPos = Vector2(defaultPos.x+moveRelativeDistance.x, defaultPos.y+moveRelativeDistance.y);
			if (toggleMe) toggle = true;
		} else {
			targetPos = defaultPos;
			if (toggleMe) toggle = false;
		}
		
		startMoving = true;
	}

}
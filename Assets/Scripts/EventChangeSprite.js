﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event change sprite in sprite renderer. 
sprite toggles between new and old sprite.

eventMessageTag : set event message tag, to only 
trigger event when specific tag is send

changeToSprite : set a target sprite

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;
public var changeToSprite : Sprite;
public var toggleSprite : boolean; 

private var defaultSprite : Sprite;
private var spriteToggle : boolean;

function Start () {

	defaultSprite = gameObject.GetComponent(SpriteRenderer).sprite;

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if (spriteToggle) {
			gameObject.GetComponent(SpriteRenderer).sprite = defaultSprite;
			if (toggleSprite) spriteToggle = false;
		} else {
			gameObject.GetComponent(SpriteRenderer).sprite = changeToSprite;
			if (toggleSprite) spriteToggle = true;
		}

	}
}
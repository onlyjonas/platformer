﻿#pragma strict

public var eventMessageTag : String;
public var scaleTo : Vector2;
public var toggleMe : boolean;
public var speed : float = 1.0;

private var defaultScale : Vector2;
private var targetScaleTo : Vector2;
private var toggle : boolean; 

function Start () {
	
	defaultScale = Vector2(transform.localScale.x, transform.localScale.y);
	targetScaleTo = defaultScale;

}

function Update () {

	var step = speed * Time.deltaTime;
	
	transform.localScale = Vector2.MoveTowards(Vector2(transform.localScale.x, transform.localScale.y), targetScaleTo, step);

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if(!toggle) {
			targetScaleTo = Vector3(scaleTo.x, scaleTo.y, 1);
			if (toggleMe) toggle = true;
		} else {
			targetScaleTo = defaultScale;
			if (toggleMe) toggle = false;
		}
	
	}
}
﻿#pragma strict
/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On Event...
* * * * * * * * * * * * * * * * * * * * * * * * * * * */


public var eventMessageTag : String;
public var textMeshPrefab : Transform;
public var offsetPosition : Vector2;
public var textDisplayTime : float;
public var texte : String[];

private var textMesh : Transform;
private var textIndex : int;
private var myTimer : float; 

function Awake () {

	if(!textMeshPrefab) textMeshPrefab = Resources.Load("TextMeshPrefab", Transform);

}


function Start () {
	textIndex = 0;
	
	textMesh = Instantiate (textMeshPrefab, Vector3(	transform.position.x + offsetPosition.x,
														transform.position.y + offsetPosition.y,
														transform.position.z), Quaternion.identity);						
	textMesh.parent = transform;
	
}

function Update() {

	if (textMesh.GetComponent(TextMesh).text) {
		
		if(myTimer > 0) myTimer -= Time.deltaTime;
		else textMesh.GetComponent(TextMesh).text = null;
			
		
	}

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		//create object based on textMeshPrefab to display text
		textMesh.GetComponent(TextMesh).text = texte[textIndex];
		myTimer = textDisplayTime;
		
		if(textIndex < texte.length-1) textIndex++;
		else textIndex = 0;
	
	}
}
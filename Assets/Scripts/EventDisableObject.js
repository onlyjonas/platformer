﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event disable object

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;

public var disableRenderer : boolean;
public var disableCollider : boolean;
public var disableRigidbody : boolean;

public var useActivitionTimer : boolean;
public var timeTillActivision : float;


function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if(disableRenderer) renderer.enabled = false;
		if(disableCollider) transform.collider2D.enabled = false;
		if(disableRigidbody) transform.rigidbody2D.isKinematic = true;
		
		if (useActivitionTimer) {
			yield WaitForSeconds(timeTillActivision);
		
			renderer.enabled = true;
			transform.collider2D.enabled = true;
			transform.rigidbody2D.isKinematic = false;
		}
		
	}
}
﻿#pragma strict
@script RequireComponent(Collider2D)

/* SENSOR SCRIPT * * * * * * * * * * * * * * * * * * * *
Calls an event if another object enters trigger collider
Note: activate Trigger in Collider2d 

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var useNameDetection : boolean;
public var detectionName : String;
public var onlyOnce : boolean;
public var eventMessageTag : String;
public var sendEventToMe : boolean = true;
public var sendEventToTriggerObj : boolean;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var once : boolean;

function Start () {
	once = true;
}

function OnTriggerEnter2D(other : Collider2D) {

	if(useNameDetection) {
	
		if( other.gameObject.name == detectionName  && once ) {
			SendMyEvent( other.gameObject );		
			if(onlyOnce) once = false;
		}
	
	} else {
		
		if( once ) {
			SendMyEvent( other.gameObject);	
			if(onlyOnce) once = false;
		}

	}	
	
}

function SendMyEvent( otherObj : GameObject ) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent", eventMessageTag);
		if(sendEventToTriggerObj) otherObj.SendMessage("MyEvent", eventMessageTag);

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent", eventMessageTag);
		}

	

}
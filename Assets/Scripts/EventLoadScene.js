﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event load nextLevel

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;
public var nextLevelID : int;

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		Application.LoadLevel (nextLevelID);

	}
}
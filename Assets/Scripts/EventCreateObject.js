﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event create object

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;
public var prefab : Transform;
public var position : Vector2;
public var randomPosition : boolean;
public var randomArea : Rect = Rect (-2.5, 5, 5, 5);

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if (randomPosition) position = Vector2(	Random.Range(randomArea.x, randomArea.x+randomArea.width), 
												Random.Range(randomArea.y, randomArea.y-randomArea.height));

		Instantiate (prefab, Vector3(position.x ,position.y, 0), prefab.rotation);
	
	}

}
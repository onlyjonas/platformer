﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
On event destroy object

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		Destroy (gameObject);

	}
}
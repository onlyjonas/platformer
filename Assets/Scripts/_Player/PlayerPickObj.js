﻿#pragma strict
@script RequireComponent(Collider2D)

public var pickableObjTag : String = "Pickable";
public var objTaken : GameObject;

private var newObj : GameObject;	

function Awake () {
	transform.collider2D.enabled = false;
}


function TakeObj () {
	
	transform.collider2D.enabled = true;
	yield;
	transform.collider2D.enabled = false;
	
	if(objTaken) {
		objTaken.transform.position = transform.position;
		objTaken.SetActive(true);
		objTaken = null;
	}
	
	if(newObj) {
		objTaken = newObj;
		newObj = null;
	}
	
}


function OnTriggerEnter2D(other: Collider2D) {

	if (other.tag == pickableObjTag) {	
		other.gameObject.SetActive (false);
		newObj = other.gameObject;		
	}
	
	transform.collider2D.enabled = false;
	
}
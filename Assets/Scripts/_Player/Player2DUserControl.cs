﻿using UnityEngine;

[RequireComponent(typeof(PlayerCharacter2D))]
public class Player2DUserControl : MonoBehaviour 
{
	private PlayerCharacter2D character;
    private bool jump;
	private bool take; 


	void Awake()
	{
		character = GetComponent<PlayerCharacter2D>();
	}

    void Update ()
    {
        // Read the jump input in Update so button presses aren't missed.
		#if CROSS_PLATFORM_INPUT
        if (CrossPlatformInput.GetButtonDown("Jump")) jump = true;
		#else
		if (Input.GetButtonDown("Jump")) jump = true;
		if (Input.GetButtonDown ("Fire2")) {
			take = true;
			BroadcastMessage("TakeObj");

		}
		if (Input.GetButtonUp ("Fire2")) {
			take = false;
		}
		#endif

    }

	void FixedUpdate()
	{
		// Read the inputs.
		bool crouch = Input.GetButton ("Fire1");
		#if CROSS_PLATFORM_INPUT
		float h = CrossPlatformInput.GetAxis("Horizontal");
		#else
		float h = Input.GetAxis("Horizontal");
		#endif

		// Pass all parameters to the character control script.
		character.Move( h, crouch , jump, take );

        // Reset the jump input once it has been used.
	    jump = false;
	}
}

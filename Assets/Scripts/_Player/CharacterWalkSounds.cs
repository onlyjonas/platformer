﻿using UnityEngine;
using System.Collections;

public class CharacterWalkSounds : MonoBehaviour {
	
	public AudioClip[] walkSounds = new AudioClip[3];

	public float minSpeed = 0.3f;
	public float audioStepLength = 0.5f;

	private PlayerCharacter2D character;
	
	
	void Awake()
	{
		character = GetComponent<PlayerCharacter2D>();
	}
	

	// Use this for initialization
	void Update () {
		StartCoroutine(PlayWalkSound ());
	}

	private bool isPlaying = false;

	IEnumerator PlayWalkSound() {


		if (character.grounded && character.transform.rigidbody2D.velocity.magnitude > minSpeed) {

			if(!isPlaying) {

				isPlaying = true;
					
				//Spawn a new object with AudioSource and Clip set, at a point in the 3D world
				AudioSource.PlayClipAtPoint (walkSounds [Random.Range (0, walkSounds.Length)], transform.position);
		
				yield return new WaitForSeconds (audioStepLength);

				isPlaying = false;
					
					
			}

		}
	
	}
		
}



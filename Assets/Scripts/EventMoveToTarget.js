﻿#pragma strict

public var eventMessageTag : String;
public var target: Transform;
public var speed: float; 
public var toggleMe : boolean;

private var moveToggle : boolean;

function Update () {

	if(moveToggle && target) {
	
		// The step size is equal to speed times frame time.
		var step = speed * Time.deltaTime;
		
		// Move our position a step closer to the target.
		transform.position = Vector3.MoveTowards(transform.position, target.position, step);
	
	}
}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if(!moveToggle) {		
			moveToggle = true;
		} else {
			if (toggleMe) moveToggle = false;
		}

	}

}
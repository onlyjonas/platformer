﻿#pragma strict
public var eventMessageTag : String;
public var rotationSpeed : float;
public var changeDirectionOnEvent : boolean;

private var myDir : float = 1.0;

function Update () {

	transform.Rotate(0.0 , 0.0 , rotationSpeed * 10 * myDir * Time.deltaTime);
}

function MyEvent ( eventMsg : String ) {
	
	if(eventMsg == eventMessageTag) {
	
		if (changeDirectionOnEvent) myDir *= -1;
	
	}
}
﻿#pragma strict

public var eventMessageTag : String;
public var rotateTo : float;
public var toggleMe : boolean;
public var speed : float = 1.0;

private var defaultRotation : Vector3;
private var targetRotationAngle : float;
private var toggle : boolean; 

function Start () {
	defaultRotation = transform.eulerAngles;
	targetRotationAngle = defaultRotation.z;
}


function Update () {

	// easing	
	var x : float = ((targetRotationAngle - transform.eulerAngles.z) * speed);
	transform.eulerAngles.z += x;

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {

		if(!toggle) {
			targetRotationAngle = rotateTo;
			if (toggleMe) toggle = true;
		} else {
			targetRotationAngle = defaultRotation.z;
			if (toggleMe) toggle = false;
		}
	}

}
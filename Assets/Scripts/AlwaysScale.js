﻿#pragma strict
public var eventMessageTag : String;
public var scaleSpeed : Vector2;
public var changeDirectionOnEvent : boolean;

private var myDir : float = 1.0;

function Update () {

	transform.localScale += Vector3(scaleSpeed.x*myDir, scaleSpeed.y*myDir, 0) * Time.deltaTime;

}

function MyEvent ( eventMsg : String ) {
	
	if(eventMsg == eventMessageTag) {
		if (changeDirectionOnEvent) myDir *= -1;
	}
}
﻿#pragma strict

public var maxYPosition : float;
public var eventMessageTag : String;
public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];


function Update () {

	// check if object is outsite max position 
	if(transform.position.y < maxYPosition) SendMyEvent();
}

function SendMyEvent( ) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent", eventMessageTag);

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent", eventMessageTag);
		}

}
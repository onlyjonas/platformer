﻿#pragma strict

public var eventRecieveMessageTag : String;
public var countUpTill : int;

public var eventSendMessageTag : String;
public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

public var currentCounterDontChange : int;

function Start () {
	currentCounterDontChange = 0;
}

function Update () {

}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventRecieveMessageTag && currentCounterDontChange < countUpTill) {
	
		currentCounterDontChange++;
	
		if (currentCounterDontChange == countUpTill ) SendMyEvent();
	}
	
}

function SendMyEvent() {

		if(sendEventToMe) gameObject.SendMessage("MyEvent", eventSendMessageTag);

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent", eventSendMessageTag);
		}

}
﻿#pragma strict

/* EVENT SCRIPT * * * * * * * * * * * * * * * * * * * *
Resets object position to start position

eventMessageTag : set event message tag, to only 
trigger event when specific tag is send

timeBeforeReset : set a delay time befor object resets

* * * * * * * * * * * * * * * * * * * * * * * * * * * */

public var eventMessageTag : String;		
public var timeBeforeReset : float;

private var startPosition : Vector3;

function Start () {
	startPosition = transform.position;
}

function MyEvent ( eventMsg : String ) {

	if(eventMsg == eventMessageTag) {
		
		renderer.enabled = false;
		if(transform.collider2D) transform.collider2D.enabled = false;
		if(transform.rigidbody2D) transform.rigidbody2D.isKinematic = true;
		
		yield WaitForSeconds(timeBeforeReset);
		
		renderer.enabled = true;
		if(transform.collider2D) transform.collider2D.enabled = true;
		if(transform.rigidbody2D)transform.rigidbody2D.isKinematic = false;
		
		transform.position = startPosition;
		
	}

}
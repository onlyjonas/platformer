# Platformer Framework #

This is a little unity framework to build platformer games. With the framework you should be able to create puzzle-like interactive scenario.

The framework is part of my game design course at the cologne game lab: http://j-hansen.de/gdp. The focus of this framework is to provide an easy construction kid, that should enable students to try out game design ideas without programming everything.

Feel free to play and create stuff with it! Let me know if you succeed!
Jonas

## Credits ##

The MIT License (MIT)
Copyright (c) 2014 Jonas Hansen, pixelsix.net

The used fond is third party material, please respect the corresponding license included in the asset folder. 
* Font: Terminal Grotesque by Raphaël Bastide

The 2D player controller is based on the player controller from: 
* Sample Assets (beta), https://www.assetstore.unity3d.com/en/#!/content/14474

